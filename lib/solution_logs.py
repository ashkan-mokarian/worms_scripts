from collections import namedtuple
from typing import Type

LogEntry = namedtuple('LogEntry', 'iter time lower_bound upper_bound')