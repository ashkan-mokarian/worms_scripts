import os


class Worm(object):

    def __init__(self, gt_file_path, worm_name=None):
        """A worm must be initialized with a corresponding gt file location"""
        self.worm_name = worm_name
        if not worm_name:
            self.worm_name = os.path.basename(gt_file_path).split('-')[0]
        if not os.path.exists(gt_file_path):
            raise FileNotFoundError("ground truth file '{}' does not exist".format(gt_file_path))
        self.gt_file_name = gt_file_path
        self._gt_labels = []

    def _get_gt_labels_from_file(self):
        with open(self.gt_file_name) as f:
            self._gt_labels = [line.strip().upper() for line in f]

    @property
    def gt_labels(self):
        if not self._gt_labels:
            self._get_gt_labels_from_file()
        return self._gt_labels


class Universe(object):

    def __init__(self, file_path=None):
        self._universe_labels = None
        if os.path.isfile(file_path):
            self._universe_labels = Universe._get_universe_labels(file_path)
        elif file_path:
            raise("The universe file in {} does not exist".format(file_path))

    def _get_universe_labels(file_path) -> set:
        universe = set()
        with open(file_path) as f:
            for line in f:
                universe.add(line.strip().upper())
        return universe

    def is_valid_label(self, label: str):
        if self._universe_labels:
            return label.upper() in self._universe_labels
        else:
            return True


if __name__ == "__main__":
    worm = Worm("./dataset/example/debug/C18G1_2L1_1-NucleiNames.txt")
    universe = Universe("dataset/example/debug/universe.txt")
    assert(universe.is_valid_label("ARC_ANT_DL"))
    assert(universe.is_valid_label("ARc_anT_dL"))
    assert(not universe.is_valid_label("Ashkan"))
    universe = Universe("")
    assert (universe.is_valid_label("ashkan"))
    print("FINISH!")
