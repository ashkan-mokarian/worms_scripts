from lib.gt_provider import Worm, Universe


DEFAULT_NAMING_CONVENTIONS = {
        'solution_file_endswith': '.sol',
        'worm_pairing': '-to-',
        'log_endswith': '.log',
        'gt_endswith': '-NucleiNames.txt',
        'universe': 'universe.txt',
        'output_file': 'output/pairwise_evaluations.txt'
}


class PairwiseMatching(object):

    def __init__(self, pairwise_matchings_dict: dict, left_worm: Worm, right_worm: Worm, universe: Universe, log_entries=None, name=None):
        self._pairwise_matchings = pairwise_matchings_dict
        self._left_worm = left_worm
        self._right_worm = right_worm
        self._universe = universe
        self._log_entries = log_entries
        self._name = name
        self.result = self._evaluate()

    def _evaluate(self):
        """implicitely assumes max number of possible matchings to be equal to the number of nodes in the smaller worm,
        i.e. worm with fewer nodes"""
        valid_correct = 0
        valid_incorrect = 0
        valid_not_matched = 0
        invalid = 0

        if len(self._left_worm.gt_labels) <= len(self._right_worm.gt_labels):
            lworm = self._left_worm
            rworm = self._right_worm
            pm = self._pairwise_matchings
        else:
            lworm = self._right_worm
            rworm = self._left_worm
            pm = {v:k for k, v in self._pairwise_matchings.items()}

        for i, gt_label in enumerate(lworm.gt_labels):
            if (not self._universe.is_valid_label(gt_label)) or (gt_label not in rworm.gt_labels):
                invalid += 1
            elif i not in pm.keys():
                valid_not_matched += 1
            else:
                right_matched_label = rworm.gt_labels[pm[i]]
                if right_matched_label == gt_label:
                    valid_correct += 1
                elif gt_label in rworm.gt_labels:
                    valid_incorrect += 1
                else:
                    invalid += 1
        return valid_correct, valid_incorrect, valid_not_matched, invalid

    def percentage_results(self):
        a, b, c, d = self.result
        return a/(a+b+c), c/(a+b+c), d/(a+b+c+d)

    def __str__(self):
        def format_output_numbers(result):
            sum_valid = result[0]+result[1]+result[2]
            t1 = result[0]
            t2 = result[0]/sum_valid
            t3 = result[2]
            t4 = result[2]/sum_valid
            t5 = result[3]
            t6 = result[3]/(sum_valid+result[3])
            return t1,t2,t3,t4,t5,t6

        str  = "=========={}=============\n\n".format(self._name)
        str += "correct[among valid]={} ({:.3}%), not-matched[among valid]={} ({:.3}%), invalid[among all]={} ({:.3}%)\n".format(*format_output_numbers(self.result))
        str += "\n"
        str += "    --iter--     --lowerbound--     --upperbound--     --dual-gap--\n"
        for log_entry in self._log_entries:
            str += "{:6}({:5.0}s)     {:.5}     {:.5}     {:.5}\n".format(log_entry.iter, log_entry.time, log_entry.lower_bound, log_entry.upper_bound, log_entry.upper_bound-log_entry.lower_bound)
        return str
