import os
from itertools import combinations
import random

from lib.worm_names import WORM_NAMES
from evaluate_pairwise_experiment import evaluate_directory
from evaluate_pairwise_experiment_dagmar import evaluate_directory as evaluate_directory_dagmar
from lib.gt_provider import Universe

random.seed(0)


def create_problem_gt_file(pair_list, file_path_without_extension, pairwise_problem_dir, universe):
    problem_file = file_path_without_extension + '.mm'
    gt_file = file_path_without_extension + '.gt'

    node_index_map = {}
    index = 0
    for pair_ in pair_list:
        for node in pair_:
            if node not in node_index_map.keys():
                node_index_map[node] = index
                index += 1
    assert 0 in node_index_map.values()

    temp_string = 'c Worms name:index ='
    for k, v in node_index_map.items():
        temp_string += "   " + k.worm_name + ':' + str(v)
    with open(problem_file, 'w') as f:
        f.write(temp_string+'\n')
    with open(gt_file, 'w') as f:
        pass

    for pair_ in pair_list:
        is_reversed = False
        temp_string = 'gm '
        if node_index_map[pair_[0]] < node_index_map[pair_[1]]:
            temp_string += str(node_index_map[pair_[0]]) + ' ' + str(node_index_map[pair_[1]]) + '\n'
        elif node_index_map[pair_[1]] < node_index_map[pair_[0]]:
            temp_string += str(node_index_map[pair_[1]]) + ' ' + str(node_index_map[pair_[0]]) + '\n'
            is_reversed = True
        else:
            raise RuntimeError("cannot have two inputs with the same index")

        problem_file_starts_with = '-to-'.join([w.worm_name for w in pair_])
        pairwise_problem_file = [os.path.join(pairwise_problem_dir, f) for f in os.listdir(pairwise_problem_dir) if f.startswith(problem_file_starts_with) and os.path.join(pairwise_problem_dir, f)]
        assert (len(pairwise_problem_file) == 1)
        pairwise_problem_file = pairwise_problem_file[0]

        with open(pairwise_problem_file) as fin:
            with open(problem_file, 'a') as fout:
                fout.write(temp_string)
                for line in fin:
                    words = line.split()
                    if words[0] == 'c':
                        continue
                    if words[0] == 'p' and is_reversed:
                        fout.write(' '.join(['p', words[2], words[1], words[3], words[4]+'\n']))
                    elif words[0] == 'a' and is_reversed:
                        fout.write(' '.join(['a', words[1], words[3], words[2], words[4]+'\n']))
                    elif words[0] == 'i0' and is_reversed:
                        fout.write(' '.join(['i1', words[1], words[2], words[3]+'\n']))
                    elif words[0] == 'i1' and is_reversed:
                        fout.write(' '.join(['i0', words[1], words[2], words[3]+'\n']))
                    elif words[0] == 'n0' and is_reversed:
                        fout.write(' '.join(['n1', words[1], words[2]+'\n']))
                    elif words[0] == 'n1' and is_reversed:
                        fout.write(' '.join(['n0', words[1], words[2]+'\n']))
                    else:
                        fout.write(line)

        if is_reversed:
            lworm = pair_[1]
            rworm = pair_[0]
        else:
            lworm = pair_[0]
            rworm = pair_[1]
        lvalid_label_idx_map = {v: k for k, v in enumerate(lworm.gt_labels) if universe.is_valid_label(v)}
        rvalid_label_idx_map = {v: k for k, v in enumerate(rworm.gt_labels) if universe.is_valid_label(v)}
        gt_map = {v: rvalid_label_idx_map[k] for k, v in lvalid_label_idx_map.items() if k in rvalid_label_idx_map.keys()}
        with open(gt_file, 'a') as f:
            f.write(temp_string)
            for k, v in gt_map.items():
                f.write('{} -> {}\n'.format(k, v))


def create_mm_problem_list(pm_list, mm_size, returned_list_size, criteria_lambda_func):
    # One of the two directions of every two worms has to be better than the other in terms of the criteria_lambda_func,
    # therefore maintain the better direction just in the beginning so no weird duplicate combinations occur
    pairwise_scores = {}
    for pm in pm_list:
        # if the other direction already exists and this one is better, then just replace
        key = (pm._left_worm, pm._right_worm)
        reversed_key = (pm._right_worm, pm._left_worm)
        if reversed_key in pairwise_scores.keys():
            if criteria_lambda_func(pm) < pairwise_scores[reversed_key]:
                pairwise_scores.pop(reversed_key)
            else:
                continue
        pairwise_scores[key] = criteria_lambda_func(pm)
    assert(0 < len(pairwise_scores) <= 450)

    # keep a list of worms, since you are using object references and not names
    worm_list = set()
    for pm in pm_list:
        if pm._left_worm not in worm_list:
            worm_list.add(pm._left_worm)
        if pm._right_worm not in worm_list:
            worm_list.add(pm._right_worm)
    assert(0 < len(worm_list) <= 30)

    # let's get all combinations since the speed does not matter that much
    def get_all_combinations(size_mm: int, pairwise_scores: map, nodeset=WORM_NAMES) -> list:
        all_combs = []
        for node_subset in combinations(nodeset, size_mm):
            score = 0
            pairwise_comb_list = []
            for pairwise_comb in combinations(node_subset, 2):
                if pairwise_comb in pairwise_scores.keys():
                    score += pairwise_scores[pairwise_comb]
                    pairwise_comb_list.append(pairwise_comb)
                elif tuple(reversed(pairwise_comb)) in pairwise_scores.keys():
                    score += pairwise_scores[tuple(reversed(pairwise_comb))]
                    pairwise_comb_list.append(tuple(reversed(pairwise_comb)))
                else:
                    score = 0
                    break
            if score:
                all_combs.append((pairwise_comb_list, score))
        return all_combs

    mm_combs = get_all_combinations(mm_size, pairwise_scores, worm_list)
    mm_combs = sorted(mm_combs, key=lambda x: x[1])
    return mm_combs[:returned_list_size]


if __name__ == "__main__":
    solver_name = "dagmar"
    dataset_size= 2
    mm_size= 25
    output_dir = '/home/ashkan/workspace/PycharmProjects/worms_scripts/output/15'
    pairwise_problem_dir = '/home/ashkan/workspace/dataset/wormEval-18-10-22'
    pairwise_evaluation_dir = '/home/ashkan/workspace/experiments/pairwise_graph_matching/dagmar_sol/18-10-22'
    print('Create Multi-matching dataset. number of problems: {}. Size: {}. Path={}'.format(dataset_size, mm_size, output_dir))

    # collect the evaluation data to select the best mm(Multi-Matching) problems based on them
    if solver_name == 'dagmar':
        pairwise_evaluation_list = evaluate_directory_dagmar(pairwise_problem_dir, pairwise_evaluation_dir)
    else:
        pairwise_evaluation_list = evaluate_directory(solver_name, pairwise_problem_dir, pairwise_evaluation_dir)
    mm_problem_list = create_mm_problem_list(pairwise_evaluation_list, mm_size, dataset_size, lambda x: x._log_entries[-1].upper_bound)

    universe = Universe(os.path.join(pairwise_problem_dir, 'universe.txt'))
    for no, pair_list in enumerate(mm_problem_list):
        output_file_without_extension = os.path.join(output_dir, str(no))
        create_problem_gt_file(pair_list[0], output_file_without_extension, pairwise_problem_dir, universe)
    print("FINISHED!!!")
