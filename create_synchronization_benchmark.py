import os
import scipy.io as sio
from scipy import sparse
import numpy as np

from evaluate_pairwise_experiment import evaluate_directory
from evaluate_pairwise_experiment_dagmar import evaluate_directory as evaluate_directory_dagmar
from lib.worm_names import WORM_NAMES


def main(pairwise_problem_dir, pairwise_evaluation_dir, output_dir, solver_name, criteria_lambda_func):
    pairwise_evaluation_list = evaluate_directory_dagmar(pairwise_problem_dir, pairwise_evaluation_dir)

    # keep the best direction of every experiment pair. at the same time keep a dict to worms by name keys
    pairwise_scores = {}
    for pm in pairwise_evaluation_list:
        # if the other direction already exists and this one is better, then just replace
        key = (pm._left_worm, pm._right_worm)
        reversed_key = (pm._right_worm, pm._left_worm)
        if reversed_key in pairwise_scores.keys():
            if criteria_lambda_func(pm) < pairwise_scores[reversed_key]:  # TODO: Problem here: if criteria_lambda_func refers to energy, then lower is better, and this line assumes only correct matches criteria
                pairwise_scores.pop(reversed_key)
            else:
                continue
        pairwise_scores[key] = criteria_lambda_func(pm)
    assert(len(pairwise_scores) == 435)

    unique_pm_list = []
    for pm in pairwise_evaluation_list:
        key = (pm._left_worm, pm._right_worm)
        if key in pairwise_scores.keys():
            unique_pm_list.append(pm)
    assert(len(unique_pm_list) == 435)

    # keep a dict of worm_names to worm references
    worm_dict = {}
    for worm_name in WORM_NAMES:
        for pm in pairwise_evaluation_list:
            if worm_name == pm._left_worm.worm_name:
                worm_dict[worm_name] = pm._left_worm
            elif worm_name == pm._right_worm.worm_name:
                worm_dict[worm_name] = pm._right_worm
            else:
                continue
            break
    assert(len(worm_dict) == 30)

    # remove from here -- added just to check florians precision recall numbers, TODO: DEL HERE
    # universe = pairwise_evaluation_list[0]._universe
    # tp = 0.0
    # sum = 0.0
    # for pm in unique_pm_list:
    #     tp += pm.result[0]
    #     sum += pm.result[0]+pm.result[1]+pm.result[2]+0.0
    # tp *= 2
    # sum *= 2
    #
    # sum2 = 0.0
    # for wn, wo in worm_dict.items():
    #     for l in wo.gt_labels:
    #         if universe.is_valid_label(l):
    #             sum2 += 1
    #
    # print('this is the average==', (tp+sum2)/(sum+sum2))
    # print('FINISH')
    # till here
    def get_worm_starting_index(worm_name):
        index = 0
        for w in WORM_NAMES:
            if w == worm_name:
                break
            index += len(worm_dict[w].gt_labels)
        return index

    ms = []
    for w in WORM_NAMES:
        ms.append(len(worm_dict[w].gt_labels))

    universe = pairwise_evaluation_list[0]._universe
    d = len(universe._universe_labels)

    # here we need to first create a sparse matrix with 1s at diagonal and 1s at the location of pairwise experiment's
    # matchings and their transposed locations
    rows = []
    cols = []
    for pm in unique_pm_list:
        row_start_index = get_worm_starting_index(pm._left_worm.worm_name)
        col_start_index = get_worm_starting_index(pm._right_worm.worm_name)
        for k, v in pm._pairwise_matchings.items():
            rows.append(int(k) + row_start_index)
            cols.append(int(v) + col_start_index)
    # add the transposed
    temp = len(rows)
    rows.extend(cols)
    cols.extend(rows[:temp])
    # add the 1s on diagonal
    w_size = sum(ms)
    diag = [i for i in range(w_size)]
    rows.extend(diag)
    cols.extend(diag)

    # now build the sparse matrices
    V = np.ones(len(rows))
    I = np.asarray(rows)
    J = np.asarray(cols)
    sparse_W = sparse.coo_matrix((V, (I, J)))

    # W_gt
    rows_gt = []
    cols_gt = []
    for pm in unique_pm_list:
        row_start_index = get_worm_starting_index(pm._left_worm.worm_name)
        col_start_index = get_worm_starting_index(pm._right_worm.worm_name)
        lvalid_label_idx_map = {v: k for k, v in enumerate(pm._left_worm.gt_labels) if universe.is_valid_label(v)}
        rvalid_label_idx_map = {v: k for k, v in enumerate(pm._right_worm.gt_labels) if universe.is_valid_label(v)}
        gt_map = {v: rvalid_label_idx_map[k] for k, v in lvalid_label_idx_map.items() if
                  k in rvalid_label_idx_map.keys()}
        for k, v in gt_map.items():
            rows_gt.append(int(k) + row_start_index)
            cols_gt.append(int(v) + col_start_index)
    temp = len(rows_gt)
    rows_gt.extend(cols_gt)
    cols_gt.extend(rows_gt[:temp])
    rows_gt.extend(diag)
    cols_gt.extend(diag)
    V_gt = np.ones(len(rows_gt))
    I_gt = np.asarray(rows_gt)
    J_gt = np.asarray(cols_gt)
    sparse_W_gt = sparse.coo_matrix((V_gt, (I_gt, J_gt)))

    sio.savemat(os.path.join(output_dir, 'worms_synch.mat'), {'m':ms, 'd':d, 'W':sparse_W, 'W_gt':sparse_W_gt})
    print('FINNISH')


if __name__ == '__main__':
    solver_name = 'kol'
    output_dir = '/home/ashkan/workspace/PycharmProjects/worms_scripts/output/synch'
    pairwise_problem_dir = '/home/ashkan/workspace/dataset/wormEval-18-10-22'
    pairwise_evaluation_dir = '/home/ashkan/workspace/experiments/dagmar_sol/18-10-22'

    main(pairwise_problem_dir, pairwise_evaluation_dir, output_dir, solver_name, lambda x: x._log_entries[-1].upper_bound)

    print('FINISH!!!')