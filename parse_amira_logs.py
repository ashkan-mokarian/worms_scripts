import os
import pickle
import re
from namedlist import namedlist
from lib.worm_names import WORM_NAMES

LogEntry = namedlist('LogEntry', 'lworm_name rworm_name lowerbound upperbound correct_matches')


def is_complete_log_entry(l):
    if l.lworm_name and l.rworm_name and l.lowerbound and l.upperbound and l.correct_matches:
        return True
    return False


def parse_log(filename):
    log_entries = []
    log_entry = LogEntry('', '', 0, 0, 0)

    rx_dict = {
        'lworm_name': re.compile(r'^atlas: (.+?)$'),
        'rworm_name': re.compile(r'^target: (.+?)$'),
        'bounds': re.compile(r'^lower_bound=([-+]\d*\.\d+?)[\t]upper_bound=([-+]\d*\.\d+?)$'),
        'correct_matches': re.compile(r'^correct in case of corresponding patch names: (\d+?)$')
    }
    with open(filename) as f:
        for line in f:
            for key, rx in rx_dict.items():
                match = rx.search(line)
                if match:
                    if key == 'lworm_name':
                        if match.group(1).strip() not in WORM_NAMES:
                            continue
                        if log_entry.lworm_name:
                            raise RuntimeError("double atlas in file {} at line {}".format(filename, line))
                        else:
                            # assert match[1].strip() in WORM_NAMES, print(match[1].strip(), line, filename)
                            log_entry.lworm_name = match.group(1).strip()
                    elif key == 'bounds':
                        log_entry.upperbound = float(match.group(2))
                        log_entry.lowerbound = float(match.group(1))
                    elif key == 'correct_matches':
                        log_entry.correct_matches = int(match.group(1))
                    elif key == 'rworm_name':
                        if match.group(1).strip() not in WORM_NAMES:
                            continue
                        if not log_entry.rworm_name:
                            log_entry.rworm_name = match.group(1).strip()
                        elif log_entry.rworm_name != match.group(1):
                            if is_complete_log_entry(log_entry):
                                log_entries.append(log_entry)
                                log_entry = LogEntry(log_entry.lworm_name, match.group(1).strip(), 0, 0, 0)
        if is_complete_log_entry(log_entry):
            log_entries.append(log_entry)
    return log_entries


def parse_logs_dir(dir_name):
    log_entries = []
    for f in (f for f in os.listdir(dir_name) if f.endswith('log')):
        log_entries.extend(parse_log(os.path.join(dir_name, f)))
    return log_entries


if __name__ == '__main__':
    log_entries = parse_logs_dir('/home/ashkan/workspace/PycharmProjects/worms_scripts/experiments/amira_logs-18-10-22-1446')
    lll = sorted(log_entries, reverse=True, key=lambda x: x.correct_matches)
    print('Number of pairwise problems parsed: {} (from {} possible)'.format(len(log_entries), 30 * 29))
    # with open('./output/amira_log_entries.pkl', 'wb') as f:
    #     pickle.dump(log_entries_sorted, f)
    # with open('./output/amira_log_entries.pkl', 'rb') as f:
    #     lll = pickle.load(f)
    #     print(lll[0], len(lll))
    print("FINISHED!")
