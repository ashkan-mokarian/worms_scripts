# worms_scripts
Some Usefull random scripts for the Worms dataset so far.

## Scripts:
- **parse_amira_logs.py**
- **create_multimatching_dataset.py**: requires amira_logs.pkl file to somehow choose between the best performing pairwise
problems. Uses this to create different size_mm:clique_size multimatching problems based on copy pasting lines from
pairwise matching problem input file to just one file by adding the correct corresponding "gm index0 index1" and stack
them. **CHECK IF IT WORKS OR NOT**.

## CHECK: maybe buggy
evaluate_pairwise_experiment_dagmar: works for now, but definitely can have time iteration parsing
in logs also included, currently only last iteration.
Also have not checked yet if log parse actually works correctly. I guess for first experiment in 
worst gaps G sheet, correct matches is not correct. TODO: TODO:


## TODO:
- add class of PairwiseMatchingExperiments which has a universe member, list of worm members, and list of PairwiseMatching
and corresponding functionalities which are used throughout other functions such as create synchronization benchmark,
multimatching dataset. lots of functionalities can correspond to a list of pairwise matching problems/evaluations.
