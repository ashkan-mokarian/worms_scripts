import argparse
import math
import re
import os

from lib.solution_logs import LogEntry
from lib.gt_provider import Universe, Worm
from lib.pairwise_evaluation import PairwiseMatching, DEFAULT_NAMING_CONVENTIONS
from parse_amira_logs import parse_logs_dir


def parse_solution_file(solution_path):
    pairwise_matchings = dict()
    try:
        with open(solution_path) as f:
            for line in f:
                if "-1" in line:
                    continue
                idx_l, idx_r = (int(x.strip()) for x in line.split(" "))
                pairwise_matchings.update({idx_l: idx_r})
    except FileNotFoundError:
        raise FileNotFoundError
    return pairwise_matchings


def evaluate_directory(gt_dir, experiment_dir, experiments_must_have_strings="", naming_conventions: dict=DEFAULT_NAMING_CONVENTIONS):
    """Returns list of PairwiseMatching evaluation objects based on formatted experiment, gt, and log files

    :param naming_conventions:
    :param gt_dir: dir where gt files, e.g. WORM_NAME-NucleiNames.txt and universe.txt exist
    :param experiment_dir: dir where WORM-to-WORM.sol exist
    :param experiments_must_have_strings: reduce experiment list to the ones including all comma separated strings
    """
    # Search all files that satisfy conditions for being a solution file in experiment_dir
    must_have_list = [x.strip() for x in experiments_must_have_strings.split(',')]
    solution_file_list = [f for f in os.listdir(experiment_dir)
                          if f.endswith(naming_conventions['solution_file_endswith'])
                          and all(s in f for s in must_have_list)]

    # Search for universe in gt_dir
    try:
        universe = Universe(os.path.join(gt_dir, naming_conventions['universe']))
    except FileNotFoundError:
        raise FileNotFoundError("Couldn't find universe label files by guessing for \
                                 universe.txt(DEAFULT_SUFFIXES['universe']) in {}".format(gt_dir))

    # Create List of Log entries but this time based on amira version
    amira_single_entry_log_list = parse_logs_dir(experiment_dir)

    # Create list of experiment objects by creating worm objects before that by extracting names
    pairwise_matching_experiment_list = []
    worm_dict = dict()
    for sol_file in solution_file_list:
        l, r = [x.strip() for x in sol_file.split('.')[0].split(naming_conventions['worm_pairing'])]
        if l not in worm_dict:
            worm_dict[l] = Worm(os.path.join(gt_dir, l + naming_conventions['gt_endswith']))
        if r not in worm_dict:
            worm_dict[r] = Worm(os.path.join(gt_dir, r + naming_conventions['gt_endswith']))

        # This is different from Paul's version
        log = [LogEntry("END", "END", x.lowerbound, x.upperbound) for x in amira_single_entry_log_list if x.lworm_name == l and x.rworm_name == r]

        pairwise_matching_experiment_list.append(
            PairwiseMatching(
                parse_solution_file(os.path.join(experiment_dir, sol_file)),
                worm_dict[l], worm_dict[r], universe, log,
                name=sol_file
            )
        )

    return pairwise_matching_experiment_list


def main():
    parser = argparse.ArgumentParser(
        description="Get evaluation of pairwise matching experiments in /output/pairwise_evaluation.txt!!!")
    parser.add_argument("--gt-dir", action="store", dest="gt_dir", required=True,
                        help="ground-truth dir where WORM_NAME-NucleiNames.txt and universe.txt exist")
    parser.add_argument("--exp-dir", action="store", dest="exp_dir", required=True,
                        help="experiment dir where WORM-to-WORM.sol files exist")
    parser.add_argument("--must-have", action="store", dest="must_have",
                        help="comma separated search keywords, experiment files will include these", default="")
    parser.add_argument("--output-file", action="store", dest="output_file", default="")
    args = parser.parse_args()

    if not all([os.path.isdir(args.gt_dir), os.path.isdir(args.exp_dir)]):
        raise("One of the directories --gt-dir='{}' or --exp-dir='{}' does not exist".format(args.gt_dir, args.exp_dir))
    experiment_list = evaluate_directory(args.gt_dir, args.exp_dir, args.must_have)

    output_file = args.output_file
    if not output_file:
        output_file = os.path.join(args.exp_dir, DEFAULT_NAMING_CONVENTIONS['output_file'])
        if not os.path.exists(os.path.dirname(output_file)):
            os.makedirs(os.path.dirname(output_file))

    avg = [x.percentage_results() for x in experiment_list]
    statistics = ""
    statistics += "==========  AVERAGE STATISTICS - number of experiments: {}  ==========\n".format(
        len(experiment_list))
    statistics += "correct-valid-matches={:.2}%  -  unmatched-valid-matches={:.2}%  -  invalid-matches={:.2}%\n\n" \
        .format(*[sum(x) / float(len(x)) for x in zip(*avg)])

    with open(output_file, "w") as f:
        f.write(statistics)
        for l in experiment_list:
            f.write(l.__str__())


if __name__ == "__main__":
    main()
    print("FINISH!!!")
